package hoa.example.taco.tacos;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class Taco {


    private Long id;
    private Date createdAt;

    @Size(min = 5, message = "Taco name should be longer than 5 characters")
    private String name;

    @NotNull(message = "At least 1 ingredient is required")
    @Size(min = 1, message = "At least 1 ingredient is required")
    private List<String> ingredients;
}
