package hoa.example.taco.tacos;

import hoa.example.taco.ingredients.Ingredient;
import hoa.example.taco.ingredients.IngredientRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/design")
@Slf4j
public class DesignTacoController {

    private IngredientRepository ingredientRepository;

    @Autowired
    public DesignTacoController(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @GetMapping
    public String showDesignForm(Model model) {

        model.addAttribute("design", new Taco());

        return "design";
    }

    @ModelAttribute
    private void addTacos(Model model) {
        List<Ingredient> ingredients = ingredientRepository.findAll();

        for (Ingredient.Type type : Ingredient.Type.values()) {
            model.addAttribute(type.toString(), ingredientsByType(ingredients, type));
        }
    }

    @PostMapping
    @Validated
    public String createTaco(@Valid @ModelAttribute("design") Taco design, Errors errors, Model model) {
        if (errors.hasErrors()) {
            return "design";
        }
        log.debug("design:" + design);
        return "redirect:order/current";
    }

    private List<Ingredient> ingredientsByType(List<Ingredient> allIngredients, Ingredient.Type type) {
        return allIngredients.stream().filter((ingredient -> ingredient.getType().equals(type))).collect(Collectors.toList());
    }
}
