package hoa.example.taco.ingredients;

import java.util.List;

public interface IngredientRepository {
    List<Ingredient> findAll();
    Ingredient findOne(String id);
    Ingredient save(Ingredient ingredient);
}
